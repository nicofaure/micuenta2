var MyPushNotification = {
    senderId: "682243355524",
    init: function(){
        var pushNotification = window.plugins.pushNotification;
        pushNotification.register(app.successPushNotificationHandler, app.errorPushNotificationHandler,{"senderID":this.senderId,"ecb":"app.onNotificationGCM"});
        // var parentElement = document.getElementById(id);
        // var listeningElement = parentElement.querySelector('.listening');
        // var receivedElement = parentElement.querySelector('.received');

        // listeningElement.setAttribute('style', 'display:none;');
        // receivedElement.setAttribute('style', 'display:block;');
    },
    successPushNotificationHandler: function(result) {
        console.log(result)
    },
    errorPushNotificationHandler:function(error) {
        console.log(error);
    },
    onNotificationGCM: function(e) {
        switch(e.event){
            case 'registered':
                if (e.regid.length > 0){
                    console.log("Regid " + e.regid);
                    console.log('registration id = '+e.regid);
                }
                break;
            case 'message':
                console.log('message = '+e.message+' msgcnt = '+e.msgcnt); break;
            case 'error':
              console.log('GCM error = '+e.msg); break;
            default:
              console.log('An unknown GCM event has occurred'); break;
        }
    }
};

var app = {
    config: {
        loginPage: 'index.html'
    },
    device:'',
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        var self = this;
        this.isInstalledApp = typeof isInstalledApp === 'undefined' ? false : isInstalledApp;

        $(document).on('ready', function(){
            self.device = Platform.getDevice();
            if(self.device == "mobile"){
                if (self.isInstalledApp) {
                    jQuery.getScript('phonegap.js', function(){
                        document.addEventListener('deviceready', self.onDeviceReady, false);
                    });
                }else{
                    self.onDocumentReady();
                }   
            }else{
                location.href = "http://micuenta.telecom.com.ar";
            }
        });
    },
    initializeCommonFunctions: function(){
        Session.validate();
        PageTransition.init();
        HTMLTemplate.load(init);
    },
    onDocumentReady: function(){
        app.initializeCommonFunctions();
    },
    onDeviceReady: function() {
        app.initializeCommonFunctions();
        MyPushNotification.init();
    }
};

app.initialize();

