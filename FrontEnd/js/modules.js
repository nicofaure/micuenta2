var Session = {
	add: function(options){
		options = $.extend({
			clientNumber: 0000
		},options);
		store.set('sessionId', GUID.get());
		store.set('time', new Date());
		store.set('user', {
			username: options.username,
			clientNumber: options.clientNumber
		});
	},
	remove: function(){
		store.clear()
	},
	get: function(){
		return store.getAll();
	},
	getUser: function(){
		return store.get('user');
	},
	isLoggedIn: function(){
		return store.get('sessionId') ? true : false;
	},
	validate: function(){
		var actualPage = location.pathname.substring(location.pathname.lastIndexOf("/") + 1);
		if (!this.isLoggedIn() && actualPage != app.config.loginPage) 
			location.href = app.config.loginPage;
	}
};

var Login = {
	$el: $('.loginContent'),
	init:function(){
		$("#btn-signin",self.$el).on("click", this, this.signIn);
	},
	getFormData: function (){
		return	{	
			username: $("#username",self.$el).val(),
			password: $("#password",self.$el).val()
		}
	},
	validateForm: function(formData){
		$("#errorMessage",self.$el).empty();
		$(".input",self.$el).parent().removeClass('has-error');
		
		if(formData.username.length < 1 && formData.password.length < 1){
			this.showMessage({message:"Completa el usuario y contraseña."});
			$("#username",self.$el).parent().addClass('has-error');
			$("#password",self.$el).parent().addClass('has-error');
			return false;
		}else if(formData.username.length < 1 ){
			this.showMessage({message:"Completa el usuario"});
			$("#username",self.$el).parent().addClass('has-error');
			return false;
		}else  if(formData.password.length < 1 ){
			this.showMessage({message:"Completa la contraseña."});
			$("#password",self.$el).parent().addClass('has-error');
			return false;
		}
		return true;
	},
	showMessage: function(options){
		$("#labelMessageErrorTmpl").tmpl({message: options.message}).prependTo("#errorMessage",self.$el);
	},
	signIn: function(event){
		event.preventDefault();
		var self = event.data;

		var formData = self.getFormData();
		if(!self.validateForm(formData))
			return false;
		
		var element = this;
		$.ajax({
			url:'/login.ssox',
			type: 'GET',
			dataType:'json',
			data: formData,
			context: self,
			beforeSend: function(){
				ButtonFeedback.loading(element);	
			},
			success: self.successfulSignIn,
			error: self.error
		});
	},
	successfulSignIn: function(response){
		if(response.status == "OK"){
			if(response.isSuccess){
				ButtonFeedback.complete("#btn-signin");
				Session.add({username: this.getFormData().username});
				location.href ='client.html';
			}else{
				ButtonFeedback.error("#btn-signin");
				this.showMessage({message:"Los datos ingresados son incorrectos."});		
			}
		} else if (response.status == "NOOK")
			this.error();
	},
	error: function(){
		ButtonFeedback.error("#btn-signin");
		this.showMessage({message:"Surgió un error al intentar iniciar sesión."});
	}
};

var Logout = {
	init: function(){
		$("body").on('click', '.btn-logout', this, this.signOut);
	},
	signOut: function(event){
		event.preventDefault();
		Session.remove();	
		location.href = app.config.loginPage;
	}
};

var Profile = {
	get: function(event){
		var self = event ? event.data : this;
		$.ajax({
			url:'/GetProfiles.ssox',
			type: 'GET',
			dataType:'json',
			context: self,
			beforeSend: function(){
				Loading.show();
			},
			success: self.successfulGet,
			error: self.errorGet,
			complete: function(){
				Loading.hide();
			}
		});
	},
	successfulGet: function(response){
		if (response.status == "OK") {
			$(".profiles").empty();
			if(response.Profiles.length > 0){
				$("#profileItemTmpl").tmpl(response.Profiles).appendTo(".profiles");
				$(".profileItem").on('click', this, this.select);	
			}else{
				$("#blockquoteMessageTmpl").tmpl({
					message:"No encontramos clientes asociados...", 
					actionClass: 'btnRefresh',
					typeClass: 'text-primary'
				}).appendTo(".profiles");
				$('.btnRefresh').on('click', this, this.get);
			}
		}else {
			this.errorGet();
		}  
	},
	errorGet: function(a,b,c){
		$("#blockquoteMessageTmpl").tmpl({
			message:"Surgió un error al intentar obtener los perfiles...", 
			actionClass: 'btnRefresh',
			typeClass: 'text-danger'
		}).appendTo(".profiles");
	},
	select: function(event){
		event.preventDefault();
		var profileNumber = $('.profileItem').data('number');
		var self = event.data;

		$.ajax({
			url:'/SelectProfile.ssox',
			type: 'GET',
			dataType:'json',
			data: {profileNumber: profileNumber},
			context: self,
			success: self.successfulSelect,					
			error: self.errorSelect
		});
	},
	successfulSelect: function(response){
		if (response.status == "OK")
			location.href = "dashboard.html";
		else
			this.errorSelect();
	}, 
	errorSelect: function(a,b,c){
		Alert.show({
			message:"Error al intentar seleccionar el cliente.",
			typeClass: "danger"
		});
	}
};

var InboxModule = {
	init:function(){
		this.get();
	},
	get: function(){
		$.ajax({ 
	    	type: "GET", 
	    	dataType: "json", 
	    	url: "/Service.svc/ObtainInboxMessages/111", 
	    	context: this,
	    	success: this.successfulGet,		
		    error: this.errorGet
	  	}); 
	},
	successfulGet: function(response){
		if(response){
        	$("#inboxDataTmpl").tmpl(response).appendTo(".inboxData");
      	}else{
      		$("#blockquoteMessageWithOutActionTmpl").tmpl({
				message:"No pudimos obtener los datos...", 
				typeClass: 'text-primary'
			}).appendTo(".inboxData");
      	}
	},
	errorGet: function(a,b,c){
		$("#blockquoteMessageWithOutActionTmpl").tmpl({
			message:"No pudimos obtener los datos...", 
			typeClass: 'text-primary'
		}).appendTo(".inboxData");
	}	
};

var ClientModule = {
	init:function(){
		this.get();
	},
	get: function(){
		$.ajax({ 
	    	type: "GET", 
	    	dataType: "json", 
	    	url: "/Service.svc/MostrarDatosCliente/12345678", 
	    	context: this,
	    	success: this.successfulGet,		
		    error: this.errorGet
	  	}); 
	},
	successfulGet: function(response){
		if(response){        	
        	$("#clientDataTmpl").tmpl(response).appendTo(".clientData");

        	var name = response.Name.split(" ");
        	$(".span-logged-username").text(name[0]);
      	}else{
      		$("#blockquoteMessageWithOutActionTmpl").tmpl({
				message:"No pudimos obtener los datos...", 
				typeClass: 'text-primary'
			}).appendTo(".clientData");
      	}
	},
	errorGet: function(a,b,c){
		$("#blockquoteMessageWithOutActionTmpl").tmpl({
			message:"No pudimos obtener los datos...", 
			typeClass: 'text-primary'
		}).appendTo(".clientData");
	}
};

var TracingModule = {
	init:function(){
		this.get();
	},
	get: function(){
		$.ajax({ 
	    	type: "GET", 
	    	dataType: "json", 
	    	url: "/Service.svc/Seguimiento/1", 
	    	context: this,
	    	success: this.successfulGet,		
		    error: this.errorGet
	  	}); 
	},
	successfulGet: function(response){
		if(response){        
			$("#headerTracingDataTmpl").tmpl({}).appendTo(".tracingData");
        	$("#tracingDataTmpl").tmpl(response).appendTo(".tracingData tbody");
      	}else{
      		$("#blockquoteMessageWithOutActionTmpl").tmpl({
				message:"No pudimos obtener los datos...", 
				typeClass: 'text-primary'
			}).appendTo(".tracingData");
      	}
	},
	errorGet: function(a,b,c){
		$("#blockquoteMessageWithOutActionTmpl").tmpl({
			message:"No pudimos obtener los datos...", 
			typeClass: 'text-primary'
		}).appendTo(".tracingData");
	}
};

var OfferModule = {
	init:function(){
		this.get();
	},
	get: function(){
		$.ajax({ 
	    	type: "GET", 
	    	dataType: "json", 
	    	url: "/Service.svc/Offers/1", 
	    	context: this,
	    	success: this.successfulGet,		
		    error: this.errorGet
	  	}); 
	},
	successfulGet: function(response){
		if(response){        	
        	$.each(offers,function(index,offer){
			    if(index == 0)
			        $("#topAdsDataTmpl").tmpl(offer).appendTo(".offersContent");
			    else 
			        $("#smallAdsDataTmpl").tmpl(offer).appendTo(".offersContent");
			});
      	}
	},
	errorGet: function(a,b,c){

	}
};

var TracingModule = {
	init:function(){
		this.get();
	},
	get: function(){
		$.ajax({ 
	    	type: "GET", 
	    	dataType: "json", 
	    	url: "/Service.svc/Seguimiento/1", 
	    	context: this,
	    	success: this.successfulGet,		
		    error: this.errorGet
	  	}); 
	},
	successfulGet: function(response){
		if(response){        
			$("#headerTracingDataTmpl").tmpl({}).appendTo(".tracingData");
        	$("#tracingDataTmpl").tmpl(response).appendTo(".tracingData tbody");
      	}else{
      		$("#blockquoteMessageWithOutActionTmpl").tmpl({
				message:"No pudimos obtener los datos...", 
				typeClass: 'text-primary'
			}).appendTo(".tracingData");
      	}
	},
	errorGet: function(a,b,c){
		$("#blockquoteMessageWithOutActionTmpl").tmpl({
			message:"No pudimos obtener los datos...", 
			typeClass: 'text-primary'
		}).appendTo(".tracingData");
	}
};

