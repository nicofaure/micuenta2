var mockSource = "js/mock/";
$.mockjaxSettings.status = 200;
$.mockjaxSettings.statusText = 'OK';
$.mockjaxSettings.responseTime = 0;
$.mockjaxSettings.isTimeout = false;
$.mockjaxSettings.contentType = 'text/json';
$.mockjaxSettings.dataType = 'json';

//LOGIN-MODULE
$.mockjax({
    url: '/login.ssox',
    proxy: mockSource + 'login.json'
});

//GETPROFILEs-MODULE
$.mockjax({
    url: '/GetProfiles.ssox',
    proxy: mockSource + 'getProfiles.json'
});

//SELECTPROFILE-MODULE
$.mockjax({
    url: '/SelectProfile.ssox',
    proxy: mockSource + 'selectProfile.json'
});

//INBOX-MODULE
$.mockjax({
    url: '/Service.svc/ObtainInboxMessages/111',
    proxy: mockSource + 'inbox.json'
});

//CLIENT-MODULE
$.mockjax({
    url: '/Service.svc/MostrarDatosCliente/12345678',
    proxy: mockSource + 'client.json'
});

//TRACING-MODULE
$.mockjax({
    url: '/Service.svc/Seguimiento/1',
    proxy: mockSource + 'tracing.json',
    
});

//OFFER-MODULE
$.mockjax({
    url: '/Service.svc/Offers/1',
    proxy: mockSource + 'offers.json'
});

//USER-MODULE
$.mockjax({
    url: '/WPGetUserData.cgh',
    proxy: mockSource + 'selectProfile.json'
});

$(function(){
	//$.mockjaxClear() DESACTIVA TODAS LOS MOCKS
});